package fop.model.player;

import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScoreEntry implements Comparable<ScoreEntry> {

	private String name;
	private Date date;
	private int score;

	/**
	 * creates a score entry
	 * 
	 * @param name
	 * @param score
	 * @param date
	 */
	public ScoreEntry(String name, int score, Date date) {
		this.name = name;
		this.score = score;
		this.date = date;
	}

	/**
	 * creates a score entry via player
	 * 
	 * @param player
	 */
	public ScoreEntry(Player player) {
		this.name = player.getName();
		this.score = player.getScore();
		this.date = new Date();
	}

	/**
	 * compares the score
	 */
	@Override
	public int compareTo(ScoreEntry scoreEntry) {
		return Integer.compare(this.score, scoreEntry.score);
	}

	/**
	 * prints a new highscore entry
	 * 
	 * @param printWriter
	 * @author MagnusDierking
	 */
	public void write(PrintWriter printWriter) {
		printWriter.println(name + ";" + Long.toString(date.getTime()) + ";" + Integer.toString(score));
	}

	/**
	 * reads a score entry and checks if it is allowed. If allowed, this method
	 * creates an ScoreEntry obj. with the given data
	 * 
	 * @param line data for the score entry object
	 * @return ScoreEntry object with the given data
	 * @author MagnusDierking
	 */
	public static ScoreEntry read(String line) {
		// TODO
		String[] entrys = line.split(";");
		if (entrys.length == 3) {
			if (entrys[1].matches("\\d+") && entrys[2].matches("\\d+")) {
				return new ScoreEntry(entrys[0], Integer.parseInt(entrys[2]), new Date(Long.parseLong(entrys[1])));
			}
		}
		return null;
	}

	/**
	 * returns the current Date
	 * 
	 * @return
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * returns the name
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * returns the score
	 * 
	 * @return
	 */
	public int getScore() {
		return this.score;
	}

}