package fop.model.interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import fop.model.player.Player;
import fop.model.player.ScoreEntry;
import fop.view.components.gui.Resources;

public interface MessagesConstants {
	/**
	 * which player has to place a tile 
	 * @param name
	 * @return
	 */
	public static String playerPlacingTile(String name) {
		return "Player "+ name +", please place a tile.";
	}
	/**
	 * which player has to place a meeple
	 * @param name
	 * @return
	 */
	public static String playerPlacingMeeple(String name) {
		return "Player "+ name +", please place a meeple or skip (right mouse button)";
	}
	/**
	 * if player does not have a placing option
	 * @param name
	 * @return
	 */
	public static String playerNoPlacingMeepleOption(String name) {
		return "There was no option for "+ name +" to place a meeple. Going to next round.";
	}
	/**
	 * creates the winner message withthe list of winners
	 * @param winners
	 */
	public static void showWinner(List<Player> winners) {
		if(winners.size()==1) {
			JOptionPane.showMessageDialog(null, "Der Gewinner ist " + winners.get(0).getName(), null,
					JOptionPane.CLOSED_OPTION);
		}else {
			StringBuilder strg = new StringBuilder(); 			
			winners.stream().forEach(x->{ 
					strg.append(x.getName()+", ");
			});
			String nameofwinner = strg.toString();
			nameofwinner=nameofwinner.substring(0, nameofwinner.length()-2);
				JOptionPane.showMessageDialog(null, "Die Gewinner sind " + nameofwinner, null,
						JOptionPane.CLOSED_OPTION);
			
		}
	}
	
	/**
	 * This method creates the scoretable when there is a MasterOfCastles
	 * 
	 * @param players A list of all the current players
	 */
	public static void showScoretable_1(List<Player> players) {
		/**
		 * author: jannek_s
		 */
		
		List<Player> playerList = new ArrayList<Player>(players);
		
		HashMap<Player,Integer> castlesOfPlayers = new HashMap<Player,Integer>();
		for (Player p : playerList) {
			castlesOfPlayers.put(p, p.getAmountOfCompletedCastles());
		}
		
		String[] columnHeaders = {"You are a", "Player Name", "Castles Completed", "Score"};
		String[][] dataForRows = new String[playerList.size()][4];
		int place = 1;
		
		// adding the MasterOfCastles to the table
		Player winner = Collections.max(castlesOfPlayers.entrySet(), Map.Entry.comparingByValue()).getKey();
		dataForRows[place-1][0] = "Master!";
		dataForRows[place-1][1] = winner.getName();
		dataForRows[place-1][2] = Integer.toString(winner.getAmountOfCompletedCastles());
		dataForRows[place-1][3] = Integer.toString(winner.getScore());
		place += 1;
		
		//adding the other players to the table
		playerList.remove(winner);
		for( Player p: playerList) {
			dataForRows[place-1][0] = "Peasant!";
			dataForRows[place-1][1] = p.getName();
			dataForRows[place-1][2] = Integer.toString(p.getAmountOfCompletedCastles());
			dataForRows[place-1][3] = Integer.toString(p.getScore());
			place += 1;
		}
		
		JTable scoreTable = new JTable(dataForRows, columnHeaders);
		JScrollPane scrollPane = new JScrollPane(scoreTable);
		JOptionPane.showMessageDialog(null,scrollPane,"Mission accomplished: Master of Castles!",JOptionPane.CLOSED_OPTION);
		
	
	}
	
	/**
	 * This method creates the scoretable for when there is a master of Crown and Church
	 * 
	 * @param players A list of all the current players
	 */
	public static void showScoretable_2(List<Player> players) {
		/**
		 * author: jannek_s
		 */
		
		List<Player> playerList = new ArrayList<Player>(players);
		
		HashMap<Player,Integer> castlesOfPlayers = new HashMap<Player,Integer>();
		for (Player p : playerList) {
			castlesOfPlayers.put(p, p.getAmountOfCompletedCastles());
		}
		
		String[] columnHeaders = {"You are a", "Player Name", "Castles Completed", "Monateries Completed", "Score"};
		String[][] dataForRows = new String[playerList.size()][5];
		int place = 1;
		
		// adding the MasterOfCastles to the table
		Player winner = Collections.max(castlesOfPlayers.entrySet(), Map.Entry.comparingByValue()).getKey();
		dataForRows[place-1][0] = "Master!";
		dataForRows[place-1][1] = winner.getName();
		dataForRows[place-1][2] = Integer.toString(winner.getAmountOfCompletedCastles());
		dataForRows[place-1][3] = Integer.toString(winner.getAmountOfCompletedMonasteries());
		dataForRows[place-1][4] = Integer.toString(winner.getScore());
		place += 1;
			
		//adding the other players to the table
		playerList.remove(winner);
		for( Player p: playerList) {
			dataForRows[place-1][0] = "Peasant!";
			dataForRows[place-1][1] = p.getName();
			dataForRows[place-1][2] = Integer.toString(p.getAmountOfCompletedCastles());
			dataForRows[place-1][3] = Integer.toString(p.getAmountOfCompletedMonasteries());
			dataForRows[place-1][4] = Integer.toString(p.getScore());
			place += 1;
		}
		
		JTable scoreTable = new JTable(dataForRows, columnHeaders);
		JScrollPane scrollPane = new JScrollPane(scoreTable);
		JOptionPane.showMessageDialog(null,scrollPane,"Mission accomplished: Master of Crown and Church!",JOptionPane.CLOSED_OPTION);
		
	}
	
	
	/**
	 * Generates a string listing all winners and their score.
	 * 
	 * @return A string listing all winners and their score.
	 */

	public static String getWinnersMessage(List<Player> winners) {
		String message = "Game over! ";
		Iterator<Player> i = winners.iterator();
		StringBuilder strg = new StringBuilder();
		while (i.hasNext()) {
			Player p = i.next();
			message += p.getName();
			if (i.hasNext())
				message += " and ";
			else
				message += " won, scoring " + p.getScore() + " points.";

			strg.append(p.getName() + ", ");
			ScoreEntry scoreEntry = new ScoreEntry(p);
			Resources.getInstance().addScoreEntry(scoreEntry);
		}
		

		return message;
	}
	
	/**
	 * returns if highschore should be deleted 
	 * @return
	 */
	public static int deleteHighScore () {
		return JOptionPane.showConfirmDialog(null, "Should all entries be deleted?", "Deleting highscores", JOptionPane.YES_NO_OPTION);
	}
	/**
	 * shows if all is deleted 
	 */
	public static void sucessFullDeleted() {
		JOptionPane.showMessageDialog(null, "Entries are deleted successfully", "Deleted", JOptionPane.OK_OPTION);
	}
}
