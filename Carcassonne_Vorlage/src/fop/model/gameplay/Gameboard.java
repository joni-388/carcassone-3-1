package fop.model.gameplay;

import static fop.model.tile.FeatureType.CASTLE;
import static fop.model.tile.FeatureType.FIELDS;
import static fop.model.tile.FeatureType.MONASTERY;
import static fop.model.tile.FeatureType.ROAD;
import static fop.model.tile.Position.BOTTOM;
import static fop.model.tile.Position.BOTTOMLEFT;
import static fop.model.tile.Position.BOTTOMRIGHT;
import static fop.model.tile.Position.LEFT;
import static fop.model.tile.Position.RIGHT;
import static fop.model.tile.Position.TOP;
import static fop.model.tile.Position.TOPLEFT;
import static fop.model.tile.Position.TOPRIGHT;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import fop.base.Edge;
import fop.base.Graph;
import fop.base.Node;
import fop.model.graph.FeatureGraph;
import fop.model.graph.FeatureNode;
import fop.model.player.Player;
import fop.model.player.Players;
import fop.model.tile.FeatureType;
import fop.model.tile.Position;
import fop.model.tile.Tile;

public class Gameboard extends Observable<Gameboard> {
	// lest do this

	private Tile[][] board;
	private List<Tile> tiles;
	private FeatureGraph graph;
	private Tile newestTile;

	public Gameboard() {
		board = new Tile[144][144];
		tiles = new LinkedList<Tile>();
		graph = new FeatureGraph();
	}

	// kann nicht im konstrukor erfolgen, weil erst observer gesetzt werden muss
	public void initGameboard(Tile t) {
		newTile(t, 72, 72);
	}

	public void newTile(Tile t, int x, int y) {
		t.x = x;
		t.y = y;
		board[x][y] = newestTile = t;
		tiles.add(t);

		connectNodes(x, y);
		push(this); // pushes the new gameboard state to its observers (= GameBoardPanel)
	}

	/**
	 * Connects the nodes of all neighboring tiles facing the tile at given
	 * coordinates x, y. It is assumed that the tile is placed according to the
	 * rules.
	 * 
	 * @param x coordinate
	 * @param y coordinate
	 */
	private void connectNodes(int x, int y) {
		/**
		 * @author joni-388
		 */
		graph.addAllNodes(board[x][y].getNodes());
		graph.addAllEdges(board[x][y].getEdges());

		Tile t = board[x][y];

		// This might be helpful:
		// As we already ensured that the tile on top exists and fits the tile at x, y,
		// we know that if the feature of its top is a ROAD, the feature at the bottom
		// of the tile on top is a ROAD aswell. As every ROAD has FIELD nodes as
		// neighbours on both sides, we can connect those nodes of the two tiles. The
		// same logic applies to the next three routines.

		// Check top tile
		if (y - 1 >= 0)
			if (board[x][y - 1] != null) {
				graph.addEdge(t.getNode(TOP), board[x][y - 1].getNode(BOTTOM));
				if (t.getNode(TOP).getType() == ROAD) {
					graph.addEdge(t.getNode(TOPLEFT), board[x][y - 1].getNode(BOTTOMLEFT));
					graph.addEdge(t.getNode(TOPRIGHT), board[x][y - 1].getNode(BOTTOMRIGHT));
				}
			}
		// Check left tile
		if (x - 1 >= 0)
			if (board[x - 1][y] != null) {
				graph.addEdge(t.getNode(LEFT), board[x - 1][y].getNode(RIGHT));
				if (t.getNode(LEFT).getType() == ROAD) {
					graph.addEdge(t.getNode(TOPLEFT), board[x - 1][y].getNode(TOPRIGHT));
					graph.addEdge(t.getNode(BOTTOMLEFT), board[x - 1][y].getNode(BOTTOMRIGHT));
				}
			}

		// Check right tile
		if (x + 1 < 144)
			if (board[x + 1][y] != null) {
				graph.addEdge(t.getNode(RIGHT), board[x + 1][y].getNode(LEFT));
				if (t.getNode(RIGHT).getType() == ROAD) {
					graph.addEdge(t.getNode(TOPRIGHT), board[x + 1][y].getNode(TOPLEFT));
					graph.addEdge(t.getNode(BOTTOMRIGHT), board[x + 1][y].getNode(BOTTOMLEFT));
				}
			}

		// Check bottom tile
		if (y + 1 < 144)
			if (board[x][y + 1] != null) {
				graph.addEdge(t.getNode(BOTTOM), board[x][y + 1].getNode(TOP));
				if (t.getNode(BOTTOM).getType() == ROAD) {
					graph.addEdge(t.getNode(BOTTOMLEFT), board[x][y + 1].getNode(TOPLEFT));
					graph.addEdge(t.getNode(BOTTOMRIGHT), board[x][y + 1].getNode(TOPRIGHT));
				}
			}
	}

	/**
	 * Checks if the given tile could be placed at position x, y on the board
	 * according to the rules.
	 * 
	 * @param t The tile
	 * @param x The x position on the board
	 * @param y The y position on the board
	 * @return True if it would be allowed, false if not.
	 */
	public boolean isTileAllowed(Tile t, int x, int y) {

		/**
		 * @author joni-388
		 */
		if (board[x][y] != null)
			return false;

		// Check top tile
		if (board[x][y - 1] != null) {
			if (board[x][y - 1].getNode(BOTTOM).getType() != t.getNode(TOP).getType()) {
				return false;
			}
		}

		// Check left tile
		if (board[x - 1][y] != null) {
			if (board[x - 1][y].getNode(RIGHT).getType() != t.getNode(LEFT).getType()) {
				return false;
			}
		}

		// Check right tile
		if (board[x + 1][y] != null) {
			if (board[x + 1][y].getNode(LEFT).getType() != t.getNode(RIGHT).getType()) {
				return false;
			}
		}

		// Check bottom tile
		if (board[x][y + 1] != null) {
			if (board[x][y + 1].getNode(TOP).getType() != t.getNode(BOTTOM).getType()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks if the given tile would be allowed anywhere on the board adjacent to
	 * other tiles and according to the rules.
	 * 
	 * @param newTile The tile.
	 * @return True if it is allowed to place the tile somewhere on the board, false
	 *         if not.
	 */
	public boolean isTileAllowedAnywhere(Tile newTile) {
		/**
		 * @author joni-388
		 * 
		 */
		// Iterate over all tiles
		for (int rotations = 0; rotations < 4; rotations++) {
			for (int x = 0; x < 144; x++) {
				for (int y = 0; y < 144; y++) {
					if (board[x][y] != null) {
						// check top
						if (y - 1 >= 0) {
							if (board[x][y - 1] == null)
								if (isTileAllowed(newTile, x, y - 1))
									return true;
						}

						// check left
						if (x - 1 >= 0) {
							if (board[x - 1][y] == null)
								if (isTileAllowed(newTile, x - 1, y))
									return true;
						}

						// check right
						if (x + 1 < 144) {
							if (board[x + 1][y] == null)
								if (isTileAllowed(newTile, x + 1, y))
									return true;
						}

						// check bottom
						if (y + 1 < 144) {
							if (board[x][y + 1] == null)
								if (isTileAllowed(newTile, x, y + 1))
									return true;
						}
					}
				}
			}
			newTile.rotateRight();
		}
		// no valid position was found
		return false;
	}

	/**
	 * Calculates points for monasteries (one point for the monastery and one for
	 * each adjacent tile).
	 */
	public void calculateMonasteries(State state) {
		/**
		 * @author joni-388
		 */
		int scores = 1;
		for (int x = 0; x < 144; x++) {
			for (int y = 0; y < 144; y++) {
				if (board[x][y] != null) {
					if (board[x][y].getNode(Position.CENTER) != null) {
						if (board[x][y].getNode(Position.CENTER).getType() == MONASTERY) { // evtl �berfl�ssig
							if (board[x][y].getNode(Position.CENTER).hasMeeple()) {
								// TOP
								if (y - 1 >= 0)
									if (board[x][y - 1] != null)
										scores += 1;
								// TOPLEFT
								if (y - 1 >= 0 && x - 1 >= 0)
									if (board[x - 1][y - 1] != null)
										scores += 1;
								// TOPRIGHT
								if (y - 1 >= 0 && x + 1 < 144)
									if (board[x + 1][y - 1] != null)
										scores += 1;
								// RIGHT
								if (x + 1 < 144)
									if (board[x + 1][y] != null)
										scores += 1;
								// BOTTOMRIGHT
								if (y + 1 < 144 && x + 1 < 144)
									if (board[x + 1][y + 1] != null)
										scores += 1;
								// BOTTOM
								if (y + 1 < 144)
									if (board[x][y + 1] != null)
										scores += 1;
								// BOTTOMLEFT
								if (y + 1 < 144 && x - 1 >= 0)
									if (board[x - 1][y + 1] != null)
										scores += 1;
								// LEFT
								if (x - 1 >= 0)
									if (board[x - 1][y] != null)
										scores += 1;

								//
								if (state == State.GAME_OVER) {
									board[x][y].getMeeple().addScore(scores);
								} else if (scores == 9) {
									board[x][y].getMeeple().addScore(scores);
									//adding a point to the players total of completed Monasteries
									board[x][y].getMeeple().addCompletedMonastery();
									board[x][y].getMeeple().returnMeeple();
									board[x][y].getNode(Position.CENTER).setPlayer(null);
									
								}
								scores = 1;

							}
						}
					}
				}
			}
		}
		// the methods getNode() and getType of class Tile and FeatureNode might be
		// helpful

		// Check all surrounding tiles and add the points

		// Points are given if the landscape is complete or the game is over
		// Meeples are just returned in case of state == State.GAME_OVER

		// After adding the points to the overall points of the player, set the score to
		// 1 again
	}

	/**
	 * Calculates points and adds them to the players score, if a feature was
	 * completed. FIELDS are only calculated when the game is over.
	 * 
	 * @param state The current game state.
	 */
	public void calculatePoints(State state) {
		// Fields are only calculated on final scoring.
		if (state == State.GAME_OVER)
			calculatePoints(FIELDS, state);

		calculatePoints(CASTLE, state);
		calculatePoints(ROAD, state);
		calculateMonasteries(state);
	}

	/**
	 * Calculates and adds points to the players that scored a feature. If the given
	 * state is GAME_OVER, points are added to the player with the most meeple on a
	 * subgraph, even if it is not completed.
	 * 
	 * @param type  The FeatureType that is supposed to be calculated.
	 * @param state The current game state.
	 */
	public void calculatePoints(FeatureType type, State state) {
		/**
		 * @author joni-388
		 */
		
		if (type == FIELDS) {
			List<Node<FeatureType>> nodeList = new ArrayList<>(graph.getNodes(type));
			List<Node<FeatureType>> visitedNodes = new ArrayList<>();
			ArrayDeque<Node<FeatureType>> queue = new ArrayDeque<>();
			List<Graph<FeatureType>> completedCastles = getAllCompletedOfType(CASTLE);

			while (!nodeList.isEmpty()) {
				FeatureNode rootNode = (FeatureNode) nodeList.remove(0);
				if (!visitedNodes.contains(rootNode)) {
					if (hasMeepleOnSubGraph(rootNode)) {
						queue.push(rootNode);
						Set<Graph<FeatureType>> castelsOnField = new HashSet<Graph<FeatureType>>();
						Map<Player, Integer> playerMap = new HashMap<Player, Integer>();
						for (Player p : Players.getPLayers()) {
							playerMap.put(p, 0);
						}

						while (!queue.isEmpty()) {
							FeatureNode node = (FeatureNode) queue.pop();
							visitedNodes.add(node);
							Tile t = getNodesTile(node);
							Graph<FeatureType> g;
							switch (getNodesPosition(node)) {
							case TOPRIGHT: {
								if (!(null == (g = nodesCastelGraph(t.getNode(TOP), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(RIGHT), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case TOPLEFT: {
								if (!(null == (g = nodesCastelGraph(t.getNode(TOP), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(LEFT), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case TOP: {
								if (!(null == (g = nodesCastelGraph(t.getNode(LEFT), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(RIGHT), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case BOTTOMRIGHT: {
								if (!(null == (g = nodesCastelGraph(t.getNode(BOTTOM), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(RIGHT), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case BOTTOMLEFT: {
								if (!(null == (g = nodesCastelGraph(t.getNode(BOTTOM), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(LEFT), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case BOTTOM: {
								if (!(null == (g = nodesCastelGraph(t.getNode(LEFT), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(RIGHT), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case LEFT: {
								if (!(null == (g = nodesCastelGraph(t.getNode(TOP), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(BOTTOM), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case RIGHT: {
								if (!(null == (g = nodesCastelGraph(t.getNode(TOP), completedCastles)))) {
									castelsOnField.add(g);
								}
								if (!(null == (g = nodesCastelGraph(t.getNode(BOTTOM), completedCastles)))) {
									castelsOnField.add(g);
								}
							}
								break;
							case CENTER:
								;
								break;
							}
							if (node.hasMeeple()) {
								playerMap.put(t.getMeeple(), playerMap.get(t.getMeeple()) + 1);
							}
							List<Edge<FeatureType>> edges = graph.getEdges(node);
							for (Edge<FeatureType> e : edges) {
								Node<FeatureType> nextNode = e.getOtherNode(node);
								if (!visitedNodes.contains(nextNode)) {
									queue.push(nextNode);
								}
							}
						}
						int score = castelsOnField.size() * 3;
						Optional<Entry<Player, Integer>> maxEntry = playerMap.entrySet().stream()
								.max(Comparator.comparing(Map.Entry::getValue));
						maxEntry.get().getKey().addScore(score);
						for (Entry<Player, Integer> other : playerMap.entrySet()) {
							if (other.getValue().equals(maxEntry.get().getValue())) {
								if (other != maxEntry.get()) {
									other.getKey().addScore(score);
								}
							}
						}
					}

				}

			}
			return;
		}
		
		
		// queue defines the connected graph. If this queue is empty, every node in this
		// graph will be visited.
		// if nodeList is non-empty, insert the next node of nodeList into this queue
		List<Node<FeatureType>> nodeList = new ArrayList<>(graph.getNodes(type));
		List<Node<FeatureType>> visitedNodes = new ArrayList<>();
		ArrayDeque<Node<FeatureType>> queue = new ArrayDeque<>();
		ArrayList<Player> playerList = new ArrayList<Player>(Players.getPLayers());

		while (!nodeList.isEmpty()) {
			FeatureNode firstNode = (FeatureNode) nodeList.remove(0);
			if (!visitedNodes.contains(firstNode))
				if (hasMeepleOnSubGraph(firstNode)) {
					HashMap<Player, Integer> numberOfMeepleFromPlayerMap = new HashMap<Player, Integer>();
					for (Player a : playerList) {
						numberOfMeepleFromPlayerMap.put(a, 0);
					}
					int score = 0;
					int pennant = 0;
					boolean completed = true;
					ArrayList<Node<FeatureType>> nodesWithMeeple = new ArrayList<Node<FeatureType>>();
					Set<Tile> tilesOfSubGraph = new HashSet<Tile>();
					
					queue.push(firstNode);
					while (!queue.isEmpty()) {
						FeatureNode node = (FeatureNode) queue.pop();
						visitedNodes.add(node);
						tilesOfSubGraph.add(getTileContainingNode(node));
						
						if (node.hasMeeple()) {
							nodesWithMeeple.add(node);
							// neuer map wert ++ f�r player auf tile
							numberOfMeepleFromPlayerMap.put(getNodesTile(node).getMeeple(),
									numberOfMeepleFromPlayerMap.get(getNodesTile(node).getMeeple()) + 1);
						}

						List<Edge<FeatureType>> edges = graph.getEdges(node);
						for (Edge<FeatureType> edge : edges) {
							Node<FeatureType> nextNode = edge.getOtherNode(node);
							if (!visitedNodes.contains(nextNode)) {
								queue.push(nextNode);
							}
						}
						if (!node.isConnectingTiles()) {
							completed = false;
						}
					}
					// z�hlt anzahl der Teile
					for (Tile t : tilesOfSubGraph) {
						if (t.hasPennant()) {
							pennant++;
						}	
						score++;
					}
					// add scores
					if (state != State.GAME_OVER) {
						if (completed) {
							if (type == FeatureType.CASTLE) {
								score += pennant;
								score *= 2;
								
								
								// max map entry
								Optional<Entry<Player, Integer>> maxEntry = numberOfMeepleFromPlayerMap.entrySet().stream()
										.max(Comparator.comparing(Map.Entry::getValue));
								// set score
								maxEntry.get().getKey().addScore(score);
								// raising players completed castle score
								maxEntry.get().getKey().addCompletedCastle();
								// in case some Player have same number of meeple
								for (Entry<Player, Integer> other : numberOfMeepleFromPlayerMap.entrySet()) {
									if (other.getValue().equals(maxEntry.get().getValue())) {
										if(other != maxEntry.get()) {
											other.getKey().addScore(score);
											other.getKey().addCompletedCastle();
										}
									}
								}

							}
							else {
								// max map entry
								Optional<Entry<Player, Integer>> maxEntry = numberOfMeepleFromPlayerMap.entrySet().stream()
										.max(Comparator.comparing(Map.Entry::getValue));
								// set score
								maxEntry.get().getKey().addScore(score);
								// in case some Player have same number of meeple
								for (Entry<Player, Integer> other : numberOfMeepleFromPlayerMap.entrySet()) {
									if (other.getValue().equals(maxEntry.get().getValue())) {
										if(other != maxEntry.get()) {
											other.getKey().addScore(score);
										}
									}
								}
							}
							// return meeple
							while (!nodesWithMeeple.isEmpty()) {
								FeatureNode a = (FeatureNode) nodesWithMeeple.remove(0);
								Player player = a.getPlayer();
								a.setPlayer(null);
								player.returnMeeple();
							}
						}
					} else {
						if (type == FeatureType.CASTLE) {
							score += pennant;

						}else if (type == FeatureType.FIELDS) {
							//score /= 4;

						}
						// max map entry
						Optional<Entry<Player, Integer>> maxEntry = numberOfMeepleFromPlayerMap.entrySet().stream()
								.max(Comparator.comparing(Map.Entry::getValue));
						// set score
						maxEntry.get().getKey().addScore(score);
						// in case some Player have same number of meeple
						for (Entry<Player, Integer> other : numberOfMeepleFromPlayerMap.entrySet()) {
							if (other.getValue().equals(maxEntry.get().getValue())) {
								if(other != maxEntry.get()) {
									other.getKey().addScore(score);
								}
							}
						}

					}

				}

		}

		// int score = 0;
		// boolean completed = true; // Is the feature completed? Is set to false if a
		// node is visited that does not
		// connect to any other tile

		// queue.push(nodeList.remove(0));
		// Iterate as long as the queue is not empty
		// Remember: queue defines a connected graph

		// Hint:
		// If there is one straight positioned node that does not connect to another
		// tile, the feature cannot be completed.

	}

	/**
	 * returns all completed Subgraphs of a type in a List
	 * 
	 * @param type
	 * @return List<Graph<FeaturType>>
	 * @author joni-388
	 */
	private List<Graph<FeatureType>> getAllCompletedOfType(FeatureType type) {
		List<Node<FeatureType>> nodeList = new ArrayList<>(graph.getNodes(type));
		List<Node<FeatureType>> visitedNodes = new ArrayList<>();
		ArrayDeque<Node<FeatureType>> queue = new ArrayDeque<>();

		List<Graph<FeatureType>> subGraphList = new ArrayList<>();

		while (!nodeList.isEmpty()) {
			FeatureNode rootNode = (FeatureNode) nodeList.remove(0);
			if (!visitedNodes.contains(rootNode)) {
				queue.push(rootNode);
				boolean completed = true;
				Graph<FeatureType> subGraph = new Graph<FeatureType>();
				List<FeatureNode> nodesSubGraph = new ArrayList<FeatureNode>();
				List<Edge<FeatureType>> edgesSubGraph = new ArrayList<Edge<FeatureType>>();
				while (!queue.isEmpty()) {
					FeatureNode node = (FeatureNode) queue.pop();
					visitedNodes.add(node);
					nodesSubGraph.add(node);

					if (!node.isConnectingTiles())
						completed = false;

					List<Edge<FeatureType>> edges = graph.getEdges(node);
					edgesSubGraph.addAll(edges);
					for (Edge<FeatureType> e : edges) {
						Node<FeatureType> nextNode = e.getOtherNode(node);
						if (!visitedNodes.contains(nextNode)) {
							queue.push(nextNode);
						}
					}
				}
				if (completed) {
					subGraph.addAllEdges(edgesSubGraph);
					subGraph.addAllNodes(nodesSubGraph);
					subGraphList.add(subGraph);
				}

			}
		}
		return subGraphList;
	}
	/**
	 * Method returns Graph of node's castle else null
	 * @param node
	 * @param castles
	 * @return graph of nodes catle
	 * @author joni-388
	 */
	private Graph<FeatureType> nodesCastelGraph(FeatureNode node, List<Graph<FeatureType>> castles) {
		for (Graph<FeatureType> g : castles) {
			if (g.containsNode(node)) {
				return g;
			}

		}
		return null;
	}

	/**
	 * Returns node's position on its tile
	 * 
	 * @param node
	 * @return position
	 * @author joni-388
	 */
	private Position getNodesPosition(FeatureNode node) {
		Tile t = getNodesTile(node);
		if (t.getNode(TOPRIGHT) == node)
			return TOPRIGHT;
		if (t.getNode(TOPLEFT) == node)
			return TOPLEFT;
		if (t.getNode(TOP) == node)
			return TOP;
		if (t.getNode(BOTTOM) == node)
			return BOTTOM;
		if (t.getNode(BOTTOMRIGHT) == node)
			return BOTTOMRIGHT;
		if (t.getNode(BOTTOMLEFT) == node)
			return BOTTOMLEFT;
		if (t.getNode(Position.CENTER) == node)
			return Position.CENTER;
		if (t.getNode(RIGHT) == node)
			return RIGHT;
		if (t.getNode(LEFT) == node)
			return LEFT;
		return null;
	}
	/**
	 * @author joni-388 Mehtod returns nodes Tile
	 * @param node
	 * @return Tile
	 */
	private Tile getNodesTile(FeatureNode node) {
		for (int x = 0; x < 144; x++) {
			for (int y = 0; y < 144; y++) {
				if (board[x][y] != null)
					if (board[x][y].containsNode(node))
						return board[x][y];
			}
		}

		return null;
	}

	/**
	 * Returns all Tiles on the Gameboard.
	 * 
	 * @return all Tiles on the Gameboard.
	 */
	public List<Tile> getTiles() {
		return tiles;
	}

	/**
	 * Returns the Tile containing the given FeatureNode.
	 * 
	 * @param node A FeatureNode.
	 * @return the Tile containing the given FeatureNode.
	 */
	private Tile getTileContainingNode(FeatureNode node) {
		for (Tile t : tiles) {
			if (t.containsNode(node))
				return t;
		}
		return null;
	}

	/**
	 * Returns the spots on the most recently placed tile on which it is allowed to
	 * place a meeple .
	 * 
	 * @return The spots on which it is allowed to place a meeple as a boolean array
	 *         representing the tile split in nine cells from top left, to right, to
	 *         bottom right. If there is no spot available at all, returns null.
	 */
	public boolean[] getMeepleSpots() {
		boolean[] positions = new boolean[9];
		boolean anySpot = false; // if there is not a single spot, this remains false

		for (Position p : Position.values()) {
			FeatureNode n = newestTile.getNodeAtPosition(p);
			if (n != null)
				if (n.hasMeepleSpot() && !hasMeepleOnSubGraph(n))
					positions[p.ordinal()] = anySpot = true;
		}

		if (anySpot)
			return positions;
		else
			return null;
	}

	/**
	 * Checks if there are any meeple on the subgraph that FeatureNode n is a part
	 * of.
	 * 
	 * @param n The FeatureNode to be checked.
	 * @return True if the given FeatureNode has any meeple on its subgraph, false
	 *         if not.
	 */
	public boolean hasMeepleOnSubGraph(FeatureNode n) {
		List<Node<FeatureType>> visitedNodes = new ArrayList<>();
		ArrayDeque<Node<FeatureType>> queue = new ArrayDeque<>();

		queue.push(n);
		while (!queue.isEmpty()) {
			FeatureNode node = (FeatureNode) queue.pop();
			if (node.hasMeeple())
				return true;

			List<Edge<FeatureType>> edges = graph.getEdges(node);
			for (Edge<FeatureType> edge : edges) {
				Node<FeatureType> nextNode = edge.getOtherNode(node);
				if (!visitedNodes.contains(nextNode)) {
					queue.push(nextNode);
					visitedNodes.add(nextNode);
				}
			}
		}
		return false;
	}

	/**
	 * Returns the newest tile.
	 * 
	 * @return the newest tile.
	 */
	public Tile getNewestTile() {
		return newestTile;
	}

	/**
	 * Places a meeple of given player at given position on the most recently placed
	 * tile (it is only allowed to place meeple on the most recent tile).
	 * 
	 * @param position The position the meeple is supposed to be placed on on the
	 *                 tile (separated in a 3x3 grid).
	 * @param player   The owner of the meeple.
	 */
	public void placeMeeple(Position position, Player player) {
		board[newestTile.x][newestTile.y].getNode(position).setPlayer(player);
		player.removeMeeple();
	}

	public Tile[][] getBoard() {
		return board;
	}

	public FeatureGraph getGraph() {
		return this.graph;
	}

	public void setFeatureGraph(FeatureGraph graph) {
		this.graph = graph;
	}
}
