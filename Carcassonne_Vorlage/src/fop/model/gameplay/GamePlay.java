package fop.model.gameplay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fop.controller.GameController;
import fop.model.interfaces.GameConstants;
import fop.model.interfaces.GameMethods;
import fop.model.interfaces.GamePlayMethods;
import fop.model.interfaces.MessagesConstants;
import fop.model.player.Player;
import fop.model.player.Players;
import fop.model.tile.Position;
import fop.model.tile.Tile;
import fop.model.tile.TileStack;
import fop.view.components.gui.GameView;

public class GamePlay extends Observable<List<Player>> implements GamePlayMethods, GameConstants {

	private GameController gc;

	public GamePlay(GameController gc) {

		this.gc = gc;
	}

	public GameController getGameController() {
		return this.gc;
	}

	@Override
	public Tile pickUpTile() {
		return gc.getTileStack().pickUpTile();
	}

	@Override
	public void newTile(Tile t, int x, int y) {
		gc.getGameBoard().newTile(t, x, y);
	}

	@Override
	public void placeMeeple(Position position) {
		gc.getGameBoard().placeMeeple(position, currentPlayer());
		nextRound();
	}

	@Override
	public boolean isTopTileAllowed(int x, int y) {
		return gc.getGameBoard().isTileAllowed(gc.getTileStack().peekTile(), x, y);
	}

	@Override
	public void rotateTopTile() {
		gc.getTileStack().rotateTopTile();
	}

	@Override

	// checks the conditions to continue to the next round
	public void nextRound() {
		if (!currentPlayer().getName().equals("AI"))
			gc.getGameBoardPanel().removeTempMeepleOverlay();
		if (gc.getTileStack().remainingTiles() == 0)
			gc.setState(State.GAME_OVER);
		else {
			gc.getGameBoard().calculatePoints(gc.getState());
			gc.getGameBoard().push(gc.getGameBoard());
			// finishes game if a mission is accomplished
			if (masterOfCastles(3) == true)
				gc.setState(State.GAME_OVER);
			else if (masterOfCrownAndChurch())
				gc.setState(State.GAME_OVER);
			else {
				gc.incrementRound();
				gc.setState(State.PLACING_TILE);
			}
		}
	}

	/**
	 * This method checks if any player completed 3 castles more, than any other
	 * player and returns true if he did. Otherthise it returns fals
	 * 
	 * @return Boolean
	 */
	public Boolean masterOfCastles(int n) {
		/**
		 * author: jannek_s
		 */

		ArrayList<Player> playerList = new ArrayList<Player>(Players.getPLayers());
		ArrayList<Integer> completedCastleList = new ArrayList<Integer>();
		for (Player p : playerList) {
			completedCastleList.add(p.getAmountOfCompletedCastles());
		}
		Collections.sort(completedCastleList, Collections.reverseOrder());
		Integer[] castleScores = completedCastleList.toArray(new Integer[completedCastleList.size()]);
		if (castleScores[0] >= n + castleScores[1]) {
			return true;
		}
		return false;

	}

	/**
	 * This method checks if a player completed at least 2 monasteries and 2 castles
	 * and returns true if he did and false if he didn't.
	 * 
	 * @return Boolean
	 */
	public Boolean masterOfCrownAndChurch() {
		/**
		 * author: jannek_s
		 */
		
		Boolean enoughCastles = masterOfCastles(2);
		
		if(enoughCastles) {
			ArrayList<Player> playerList = new ArrayList<Player>(Players.getPLayers());
			HashMap<Player,Integer> castlesOfPlayers = new HashMap<Player,Integer>();
			for (Player p : playerList) {
				castlesOfPlayers.put(p, p.getAmountOfCompletedCastles());
			}
			// the player with the most castles completed
			Player castleMaster = Collections.max(castlesOfPlayers.entrySet(), Map.Entry.comparingByValue()).getKey();
			if(castleMaster.getAmountOfCompletedMonasteries() >= 2)
				return true;
			}
		return false;
	}

	@Override
	public void initGameboard() {
		gc.getGameBoard().initGameboard(gc.getTileStack().pickUpTile());
	}

	@Override
	public void setupObservers() {
		addObserver(gc.getGameView().getToolbarPanel());
		gc.getTileStack().addObserver(gc.getTileStackPanel());
		gc.getTileStack().addObserver(gc.getGameBoardPanel().getTileOverlay());
		gc.getGameBoard().addObserver(gc.getGameBoardPanel());
	}

	@Override
	public void setupListeners() {
		gc.getGameView().getToolbarPanel().addToolbarActionListener((event) -> {
			switch (event.getActionCommand()) {
			case "Main menu":
				GameMethods.GoToMainMenu();
				break;
			case "Skip":
				nextRound();
				break;
			case "Stop":
				gc.setState(State.GAME_OVER);

			}
		});
	}

	@Override

	public Player currentPlayer() {
		return gc.getPlayers().get(gc.getCurrentRound() % gc.getPlayers().size());
	}

	@Override
	public void initGame() {
		gc.setPlayers(Players.getPLayers());
		gc.setBoard(new Gameboard());
		gc.setTileStack(new TileStack());
		GameView view = new GameView(gc);
		gc.setGameView(view);
		gc.setView(view);
		gc.setGameBoardPanel(view.getGameBoardPanel());
		gc.setTileStackPanel(view.getTileStackPanel());
		setupListeners();
		setupObservers();
		initGameboard();
		gc.setState(State.PLACING_TILE);
	}

	@Override
	public void placing_Tile_Mode() {
		push(gc.getPlayers()); // push players to observers (= ToolbarPanel)

		// According to the rules, a tile that does not fit anywhere is not mixed into
		// the stack again, but simply discarded.
		if (!gc.getGameBoard().isTileAllowedAnywhere(gc.getTileStack().peekTile())) {
			gc.getTileStack().discardTopTile();
		}

		gc.getTileStack().push(gc.getTileStack()); // pushes tile stack to observers (= TileStackPanel)
		if (currentPlayer().getName().equals("AI")) {
			Tile tile = gc.getTileStack().pickUpTile();
			currentPlayer().draw(this, tile);
			// gc.getTileStack().push(gc.getTileStack());
			gc.setState(State.PLACING_MEEPLE);
			return;
		}
		gc.getGameView().getToolbarPanel().showSkipButton(false);
		gc.getGameView().setStatusbarPanel(MessagesConstants.playerPlacingTile(currentPlayer().getName()),
				currentPlayer().getColor().getMeepleColor());
	}

	@Override
	public void placing_Meeple_Mode() {
		// When the current player does not have any meeple left, go to next round
		// immediately.
		if (currentPlayer().getMeepleAmount() == 0) {
			nextRound();
		} else {

			Tile newestTile = gc.getGameBoard().getNewestTile();
			boolean[] meepleSpots = gc.getGameBoard().getMeepleSpots();
			if (currentPlayer().getName().equals("AI")) {
				currentPlayer().placeMeeple(this);
			} else if (meepleSpots != null && !currentPlayer().getName().equals("AI")) {
				gc.getGameBoardPanel().showTemporaryMeepleOverlay(meepleSpots, newestTile.x, newestTile.y,
						currentPlayer());
				gc.getTileStackPanel().hideTopTile();
				gc.getGameView().getToolbarPanel().showSkipButton(true);
				gc.getGameView().setStatusbarPanel(MessagesConstants.playerPlacingMeeple(currentPlayer().getName()),
						currentPlayer().getColor().getMeepleColor());
				// Now waiting for user input
			} else {
				// If there are no possibilities of placing a meeple, skip to the next round
				// right away.
				nextRound();
			}
		}
	}

	public void game_Over_Mode() {	
		// finishing message if the first mission was accomplished
		if (masterOfCastles(3)) {
			MessagesConstants.showScoretable_1(Players.getPLayers());
			gc.getGameBoard().push(gc.getGameBoard());
			push(gc.getPlayers());
			gc.getGameView().getToolbarPanel().showSkipButton(false);
			gc.getGameView().setStatusbarPanel(MessagesConstants.getWinnersMessage(getWinners(gc.getPlayers())),
					WINNING_MESSAGE_COLOR);
			GameMethods.GoToMainMenu();
		}
		// finishing message if the second mission was accomplished
		else if (masterOfCrownAndChurch()) {

			MessagesConstants.showScoretable_2(gc.getPlayers());
			gc.getGameBoard().push(gc.getGameBoard());
			push(gc.getPlayers());
			gc.getGameView().getToolbarPanel().showSkipButton(false);
			gc.getGameView().setStatusbarPanel(MessagesConstants.getWinnersMessage(getWinners(gc.getPlayers())),
					WINNING_MESSAGE_COLOR);
			GameMethods.GoToMainMenu();
		}
		// finishing message if the game ended with no tiles left
		else {
			gc.getGameBoard().calculatePoints(gc.getState());
			gc.getGameBoard().push(gc.getGameBoard());
			push(gc.getPlayers());
			gc.getGameView().getToolbarPanel().showSkipButton(false);
			gc.getGameView().setStatusbarPanel(MessagesConstants.getWinnersMessage(getWinners(gc.getPlayers())),
					WINNING_MESSAGE_COLOR);

			MessagesConstants.showWinner(getWinners(gc.getPlayers()));
			GameMethods.GoToMainMenu();
		}
	}

	@Override
	public List<Player> getWinners(List<Player> players) {
		List<Player> winners = new LinkedList<Player>();
		int highestScore = 0;
		for (Player p : players)
			if (p.getScore() > highestScore) {
				winners = new LinkedList<Player>();
				winners.add(p);
				highestScore = p.getScore();
			} else if (p.getScore() == highestScore) {
				winners.add(p);
			}
		return winners;
	}

}
