package fop.view.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import fop.model.interfaces.GameMethods;
import fop.model.interfaces.MessagesConstants;
import fop.model.player.ScoreEntry;
import fop.view.components.View;
import fop.view.components.gui.Resources;

/**
 * HighScore Area
 *
 */
public class HighscoreView extends View {

	private JButton btnBack;
	private JButton btnClear;
	private JTable scoreTable;
	private JLabel lblTitle;
	private JScrollPane scrollPane;

	public HighscoreView(GameWindow gameWindow) {
		super(gameWindow);
	}

	@Override
	public void onResize() {
		int offsetY = 25;
		lblTitle.setLocation((getWidth() - lblTitle.getWidth()) / 2, offsetY);
		offsetY += lblTitle.getSize().height + 25;
		scrollPane.setLocation(25, offsetY);
		scrollPane.setSize(getWidth() - 50, getHeight() - 50 - BUTTON_SIZE.height - offsetY);

		btnBack.setLocation((getWidth() / 3) - (BUTTON_SIZE.width / 2), getHeight() - BUTTON_SIZE.height - 25);
		btnClear.setLocation((2 * (getWidth() / 3) - (BUTTON_SIZE.width / 2)), getHeight() - BUTTON_SIZE.height - 25);
	}

	@Override
	protected void onInit() {
		btnBack = createButton("Back");
		btnClear = createButton("Delete");
		lblTitle = createLabel("Highscores", 45, true);
		
		Resources resources = Resources.getInstance();
		
		/**
		 * @author MagnusDierking
		 */
		List<ScoreEntry> highscores = resources.getScoreEntries();   
		String[] columnHeaders = {"Place", "Player Name", "Date", "Score"};
		String[][] dataForRows = new String[highscores.size()][4];
		int place = 1;
		Color gold = new Color(218, 165, 32);
		Color silver = new Color(192, 192, 192);
		Color bronze = new Color(176, 141, 87);
		DateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss");
		if(!highscores.isEmpty()) {
		for(ScoreEntry s: highscores) {
			dataForRows[place-1][0] = Integer.toString(place);
			dataForRows[place-1][1] = s.getName();
			dataForRows[place-1][2] = dateFormat.format(s.getDate());
			dataForRows[place-1][3] = Integer.toString(s.getScore());
			place += 1;
		}}
		scoreTable = new JTable(dataForRows, columnHeaders)
		{
		    public Component prepareRenderer(
		        TableCellRenderer renderer, int row, int column)
		    {
		        Component c = super.prepareRenderer(renderer, row, column);
		        if(row == 0) {
		        	c.setBackground(gold);
		        }
		        if(row == 1) {
		        	c.setBackground(silver);
		        }
		        if(row == 2) {
		        	c.setBackground(bronze);
		        }
		        if(row > 2) {
		        	c.setBackground(Color.WHITE);
		        }

		        return c;
		    }
		};
		add(scrollPane = new JScrollPane(scoreTable));
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		if (actionEvent.getSource().equals(btnBack)) {
			GameMethods.GoToMainMenu();

		} else {
				int message = MessagesConstants.deleteHighScore();
				GameMethods.deleteHighScoreEntries(message);
		}
	}
}
