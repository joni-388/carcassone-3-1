package fop.model.player;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import fop.base.Edge;
import fop.base.Graph;
import fop.base.Node;
import fop.controller.GameController;
import fop.model.gameplay.GamePlay;
import fop.model.gameplay.Gameboard;
import fop.model.gameplay.State;
import fop.model.graph.FeatureNode;
import fop.model.interfaces.GameConstants;
import fop.model.interfaces.PlayerMethods;
import fop.model.tile.FeatureType;
import fop.model.tile.Position;
import fop.model.tile.Tile;

public class Player implements PlayerMethods {

	private MeepleColor color;
	private String name;
	private int score;
	private int meeples; // the amount of meeples
	private int completedCastles;
	private int completedMonasteries;
	private boolean addTileToMeeple = false;

	public Player(String name, MeepleColor color) {
		this.color = color;
		this.name = name;
		this.score = 0;
		this.meeples = GameConstants.NUMBER_OF_MEEPLES;
		this.completedCastles = 0;
		this.completedMonasteries = 0;
	}

	@Override
	public MeepleColor getColor() {
		return color;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void addScore(int score) {
		this.score += score;
	}

	@Override
	public int getScore() {
		return score;
	}

	@Override
	public int getMeepleAmount() {
		return meeples;
	}

	@Override
	public void removeMeeple() {
		meeples--;
	}

	@Override
	public void returnMeeple() {
		meeples++;
	}

	@Override
	public int getAmountOfCompletedCastles() {
		return completedCastles;
	}

	@Override
	public void addCompletedCastle() {
		completedCastles++;
	}

	@Override
	public int getAmountOfCompletedMonasteries() {
		return completedMonasteries;
	}

	@Override
	public void addCompletedMonastery() {
		completedMonasteries++;
	}

	/**
	 * Decides where the tile is supposed to be laid and calls placeTile to place
	 * the tile
	 * 
	 * @param gp
	 * @param tile
	 */
	public void draw(GamePlay gp, Tile tile) {
		GameController gc = gp.getGameController();
		boolean placed = false;
		boolean hasMeepleOnSubgraph = false;
		if (gc.getGameBoard().isTileAllowedAnywhere(tile)) {
			// check if the new tile can be placed next to a tile with a meeple
			// on a subgraph
			if (placed == false) {
				for (int rotations = 0; rotations < 4; rotations++) {
					tile.rotateRight();
					for (int x = 0; x < 144; x++) {
						for (int y = 0; y < 144; y++) {
							if (gc.getGameBoard().getBoard()[x][y] != null) {
								for (FeatureNode node : gc.getGameBoard().getBoard()[x][y].getNodes()) {
									if (hasMeepleOfSameColorOnSubGraph(gp, node)) {
										hasMeepleOnSubgraph = true;
										break;
									}
								}
								if (hasMeepleOnSubgraph == true) {
									if (placeTileOnPosition(gp, gc, tile, x, y) == true) {
										rotations = 4;
										x = 144;
										placed = true;
										break;
									}
								}
							}
						}
					}
				}
			}
			// check if the tile can be placed next to a tile with a meeple on it
			if (placed == false) {
				for (int rotations = 0; rotations < 4; rotations++) {
					tile.rotateRight();
					for (int x = 0; x < 144; x++) {
						for (int y = 0; y < 144; y++) {
							if (gc.getGameBoard().getBoard()[x][y] != null) {
								if (gc.getGameBoard().getBoard()[x][y].hasMeeple()
										&& gc.getGameBoard().getBoard()[x][y].getMeeple() == gp.currentPlayer()) {
									if (placeTileOnPosition(gp, gc, tile, x, y) == true) {
										rotations = 4;
										x = 144;
										placed = true;
										break;
									}
								}
							}
						}
					}
				}
			}
			// if first option is not possible the tile will be placed on the first possible
			// position
			if (placed == false) {
				placeTile(gp, gc, tile);
			}
		}
	}

	/**
	 * Places a tile on the first possible position on the map
	 * 
	 * @param gp
	 * @param gc
	 * @param tile
	 * @author simon
	 */
	private void placeTile(GamePlay gp, GameController gc, Tile tile) {
		for (int rotations = 0; rotations < 4; rotations++) {
			tile.rotateRight();
			for (int x = 0; x < 144; x++) {
				for (int y = 0; y < 144; y++) {
					if (gc.getGameBoard().getBoard()[x][y] != null) {
						// check top
						if (y - 1 >= 0) {
							if (gc.getGameBoard().getBoard()[x][y - 1] == null) {
								if (gc.getGameBoard().isTileAllowed(tile, x, y - 1)) {
									gp.newTile(tile, x, y - 1);
									x = 144;
									rotations = 4;
									return;
								}
							}
						}

						// check left
						if (x - 1 >= 0) {
							if (gc.getGameBoard().getBoard()[x - 1][y] == null) {
								if (gc.getGameBoard().isTileAllowed(tile, x - 1, y)) {
									gp.newTile(tile, x - 1, y);
									x = 144;
									rotations = 4;
									return;
								}
							}
						}

						// check right
						if (x + 1 < 144) {
							if (gc.getGameBoard().getBoard()[x + 1][y] == null) {
								if (gc.getGameBoard().isTileAllowed(tile, x + 1, y)) {
									gp.newTile(tile, x + 1, y);
									x = 144;
									rotations = 4;
									return;
								}
							}
						}

						// check bottom
						if (y + 1 < 144) {
							if (gc.getGameBoard().getBoard()[x][y + 1] == null) {
								if (gc.getGameBoard().isTileAllowed(tile, x, y + 1)) {
									gp.newTile(tile, x, y + 1);
									x = 144;
									rotations = 4;
									return;
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Places a tile on a given position on the map if possible. If it is impossible
	 * to place tile on that position placeTile() will be called
	 * 
	 * @param gp
	 * @param gc
	 * @param tile
	 * @param x
	 * @param y
	 * @author simon
	 */
	private boolean placeTileOnPosition(GamePlay gp, GameController gc, Tile tile, int x, int y) {
		for (int rotations = 0; rotations < 4; rotations++) {
			if (gc.getGameBoard().getBoard()[x][y] != null) {
				// check top
				if (y - 1 >= 0) {
					if (gc.getGameBoard().getBoard()[x][y - 1] == null) {
						if (gc.getGameBoard().isTileAllowed(tile, x, y - 1)) {
							gp.newTile(tile, x, y - 1);
							return true;
						}
					}
				}

				// check left
				if (x - 1 >= 0) {
					if (gc.getGameBoard().getBoard()[x - 1][y] == null) {
						if (gc.getGameBoard().isTileAllowed(tile, x - 1, y)) {
							gp.newTile(tile, x - 1, y);
							return true;
						}
					}
				}

				// check right
				if (x + 1 < 144) {
					if (gc.getGameBoard().getBoard()[x + 1][y] == null) {
						if (gc.getGameBoard().isTileAllowed(tile, x + 1, y)) {
							gp.newTile(tile, x + 1, y);
							return true;
						}
					}
				}

				// check bottom
				if (y + 1 < 144) {
					if (gc.getGameBoard().getBoard()[x][y + 1] == null) {
						if (gc.getGameBoard().isTileAllowed(tile, x, y + 1)) {
							gp.newTile(tile, x, y + 1);
							return true;
						}
					}
				}
			}
			tile.rotateRight();
		}
		return false;
	}

	/**
	 * Places a meeple on the most recent tile
	 * 
	 * @param gp
	 * @author simon
	 */
	public void placeMeeple(GamePlay gp) {
		GameController gc = gp.getGameController();
		Gameboard gameboard = gc.getGameBoard();
		if (gameboard.getMeepleSpots() != null) {
			boolean[] spots = gameboard.getMeepleSpots();
			Random randomSpot = new Random();
			boolean placed = false;
			boolean place = randomSpot.nextBoolean();
			int spot = randomSpot.nextInt(9);

			// always place a meeple if tile is a monastery
			if (placed == false && gameboard.getNewestTile().getNodeAtPosition(Position.CENTER) != null) {
				if (gameboard.getNewestTile().getNodeAtPosition(Position.CENTER).getType() == FeatureType.MONASTERY) {
					gp.placeMeeple(Position.CENTER);
					placed = true;

				}
			}
			// place meeple if castle is finished
			if (placed == false && getAllCompletedOfType(gp, FeatureType.CASTLE)) {
				for (Position position : Position.values()) {
					if (gameboard.getNewestTile().getNode(position) != null
							&& gameboard.hasMeepleOnSubGraph(gameboard.getNewestTile().getNode(position)) == false
							&& gameboard.getNewestTile().getNode(position).isConnectingTiles()
							&& gameboard.getNewestTile().getNode(position).getType() == FeatureType.CASTLE) {
						gp.placeMeeple(position);
						placed = true;
						break;
					}
				}
			}

			// place meeple if street is finished
			if (placed == false && getAllCompletedOfType(gp, FeatureType.ROAD)) {
				for (Position position : Position.values()) {
					if (gameboard.getNewestTile().getNode(position) != null
							&& gameboard.hasMeepleOnSubGraph(gameboard.getNewestTile().getNode(position)) == false
							&& gameboard.getNewestTile().getNode(position).isConnectingTiles()
							&& gameboard.getNewestTile().getNode(position).getType() == FeatureType.ROAD) {
						gp.placeMeeple(position);
						placed = true;
						break;
					}
				}
			}

			// if none of the above is working place meeple randomly
			if (placed == false && place == true) {
				if (spots[spot] != false && gp.getGameController().getGameBoard().hasMeepleOnSubGraph(
						gameboard.getNewestTile().getNode(Position.getAllPosition()[spot])) == false) {
					if (getMeepleAmount() <= 3 && gameboard.getNewestTile().getNode(Position.getAllPosition()[spot])
							.getType() != FeatureType.FIELDS) {
						gp.placeMeeple(Position.getAllPosition()[spot]);
						placed = true;

					}
					if (getMeepleAmount() > 3) {
						gp.placeMeeple(Position.getAllPosition()[spot]);
						placed = true;
					}
				}
			}
			// call nextRound() if no meeple can be placed
			if (placed == false) {
				gp.nextRound();
			}
		} else {
			gp.nextRound();
		}

		// if no position is allowed, you have to call nextRound() by yourself.
		// to place a meeple, call gp.placeMeeple(...).
	}

	/**
	 * Checks if there is a meeple of the same color on the subgraph. Returns true
	 * in that case, false otherwise
	 * 
	 * @param gp
	 * @param n
	 * @return
	 */
	private boolean hasMeepleOfSameColorOnSubGraph(GamePlay gp, FeatureNode n) {
		List<Node<FeatureType>> visitedNodes = new ArrayList<>();
		ArrayDeque<Node<FeatureType>> queue = new ArrayDeque<>();

		queue.push(n);
		while (!queue.isEmpty()) {
			FeatureNode node = (FeatureNode) queue.pop();
			if (node.hasMeeple() && node.getPlayer() == gp.currentPlayer() && node.getType() != FeatureType.FIELDS)
				return true;

			List<Edge<FeatureType>> edges = gp.getGameController().getGameBoard().getGraph().getEdges(node);
			for (Edge<FeatureType> edge : edges) {
				Node<FeatureType> nextNode = edge.getOtherNode(node);
				if (!visitedNodes.contains(nextNode)) {
					queue.push(nextNode);
					visitedNodes.add(nextNode);
				}
			}
		}
		return false;
	}

	/**
	 * returns true if a graph of type is finished, false otherwise
	 * 
	 * @param type
	 * @param gp
	 * @return
	 * @author simon
	 */
	private boolean getAllCompletedOfType(GamePlay gp, FeatureType type) {
		List<Node<FeatureType>> nodeList = new ArrayList<>();
		for (Position p : Position.getAllPosition()) {
			if (gp.getGameController().getGameBoard().getNewestTile().getNode(p) != null
					&& gp.getGameController().getGameBoard().getNewestTile().getNode(p).getType() == type) {
				nodeList.add(gp.getGameController().getGameBoard().getNewestTile().getNode(p));
			}
		}
		List<Node<FeatureType>> visitedNodes = new ArrayList<>();
		ArrayDeque<Node<FeatureType>> queue = new ArrayDeque<>();

		while (!nodeList.isEmpty()) {
			FeatureNode rootNode = (FeatureNode) nodeList.remove(0);
			if (!visitedNodes.contains(rootNode)) {
				queue.push(rootNode);
				boolean completed = true;

				while (!queue.isEmpty()) {
					FeatureNode node = (FeatureNode) queue.pop();
					visitedNodes.add(node);

					if (!node.isConnectingTiles())
						completed = false;

					List<Edge<FeatureType>> edges = gp.getGameController().getGameBoard().getGraph().getEdges(node);
					for (Edge<FeatureType> e : edges) {
						Node<FeatureType> nextNode = e.getOtherNode(node);
						if (!visitedNodes.contains(nextNode)) {
							queue.push(nextNode);
						}
					}
				}
				if (completed) {
					return true;
				}
			}
		}
		return false;
	}
}
